
<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use yii\widgets\Menu;
use yii\bootstrap\Nav;



$this->title = $page->title;
?>
<div class="page-user">
 <div class="row row-offcanvas row-offcanvas-right">
      <div class="col-xs-12 col-sm-9">
         <p class="pull-right visible-xs">
            <button type="button" class="btn btn-primary btn-xs" data-toggle="offcanvas"> Меню</button>
          </p>
            <?php  if($page->depth != 1 || $page->in_menu) {?>
            <h2 style="text-align: center; margin: 12px"><?= Html::a($page->title, ['page/page', 'slug' => $page->slug]) ?></h2>
        
        
                <?= $page->content ?>
                <?php if  ($page->getBehavior('galleryBehavior')->getImages()) { ?>
                <div class="row" style="margin: 40px auto; background-color: ">
            <?php foreach($page->getBehavior('galleryBehavior')->getImages() as $image) { ?>

                <div class="col-xs-6 col-md-4" style="margin-bottom: 10px "> 
                    <center>
                <?= Html::img($image->getUrl('small'), ['style' => [
                  'padding' => '10px', "background-color" => "#fff"]]) ?>
                <h4 style="margin: 20px auto"><?= $image->name ?></h4>
                <div class="border"></div>
                <p><?= $image->description ?></p>
                <?= $image->href ? '<a class="btn btn-primary" href="'.$image->href.'">Читать</a>' : ''?></center>
               
               </div>

         <?php  } ?>
        
 </div>
      <?php  } ?>
            <?php
             if($page->getChildren()) { ?>
        <div class="row">
       <?php foreach ($page->getChildren()->all() as $children2) {?>
                 <?php if ($children2->content) { ?>
             
                      
                       <div class="col-sm-6 col-md-4">
                        <div class="thumbnail">
                        <?php foreach($children2->getBehavior('galleryBehavior')->getImages() as $image) { ?>
                          <?= Html::img($image->getUrl('small')) ?>
                         <?php  break; } ?>
                        <div class="caption">
                        <h3><?= $children2->title ?></h3>
                      <p><?= strip_tags(mb_substr($children2->content,0,150,'UTF-8'))?>...</p>
                          <p><?= Html::a('Читать',['page/page', 'slug'=>$children2->slug],['class' =>'btn btn-default'])?></p>
      </div>
    </div>
  </div>

               <?php  } else { ?>
                   <h3 style="text-align: center; margin: 12px"><?= $children2->title ?></h3>
                <div class="row" style="margin: 40px auto ">
            <?php foreach($children2->getBehavior('galleryBehavior')->getImages() as $image) {
             if (mb_strlen($image->description) > 70) {?>
                      <div class="media">
  <a class="pull-left" href="#">
   <?= Html::img($image->getUrl('small'),['class' => 'edia-object']) ?> 
  </a>
  <div class="media-body">
    <h4 class="media-heading"><?= $image->name ?></h4>
    <div style="width: 100%; border:2px solid #343F64; margin: 10px auto"></div>
                <p><?= $image->description ?></p>
     <p></p>
  </div>
</div>
<?php  }  else { ?>
                  
                 <div class="col-xs-12 col-sm-4"> 
                    <center>
                <?= Html::img($image->getUrl('small')) ?>
                <h4 style="margin: 20px auto"><?= $image->name ?></h4>
                <p><?= $image->description ?></p>
                </center>
               </div>

    

     <?php   
    } }
    ?></div>
<?php   
    }} ?>
  </div>
 <?php   }}    ?>

</div>
 
        <div class="col-xs-6 col-sm-3 sidebar-offcanvas" id="sidebar" role="navigation">
   <?php
    echo Menu::widget([
         'activeCssClass' => 'active',
        'options' => ['class' => 'nav nav_page'],
    'items' => $menu
]);


  if ($page->getLinks()->count()) {?>
    <div style="padding: 5px; background-color: white">
     <h4 style="margin-top: 7px; text-align: center;" > Задания, ссылки</h4>

  <?php  foreach ($page->getLinks()->all() as $link ) { ?>
          <a href="<?= $link->link ?>" style="text-decoration: underline; padding-top:4px "> <?= $link->title ?> </a>
 
  <?php  }?>
</div>
  <?php  }?>


        </div><!--/span-->
</div>
</div>


<?php
 Modal::begin(['id'=>'mymodal', 'header' => '<h4>Глоссарий. Раздел "'.$page->title .'"</h4>'])?>
<?php
 echo "<div id='modalContent'></div>";
Modal::end()?>