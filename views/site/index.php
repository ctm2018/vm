<?php
use yii\helpers\Html;
use rmrevin\yii\fontawesome\FA;

/* @var $this yii\web\View */

$this->title = 'Главная';
?>
<div class="site-index">
   <div class="row" style="background-color: #FFF">
    <div class="col-md-9  jb">
        <h1>Добро пожаловать <br /> в "Виртуальный музей" </h1>

        <p class="lead">

Посещая экспозиции музея, 
Вы совершите экскурс в историю не только Гродненской специальной школы-интерната 
для детей с нарушением слуха,
но и в историю и культуру неслышащих всего мира, 
познакомитесь с историей сурдопедагогики.</p>

</div>

 <div class="col-md-3 update-data">
                <h2>Последние обновления</h2>

                   <?php foreach ($pages->findPagesDateUpdate() as $page_u) { ?>
                   <h4><?= $page_u->title ?></h4>
                   <p><?=  Yii::$app->formatter->asDate($page_u->updated_at, 'long'); ?> </p>
                  
                  <?= Html::a('Смотреть',['page/page', 'slug'=>$page_u->slug],
                  ['class'=> 'btn btn-default'])?>
              <?php }?>
            </div>
</div>

    <div class="body-content">

             <?php foreach ($pages->findPagesLimit() as $key => $page) { ?>
             <div class="row" style=" background-color: <?= $key%2 == 0 ? "#E9DB89" : "#FFF"?>">
            <div class="col-md-12" style="margin: 25px 0" >
                <center>
                <h2><?= FA::icon($page->icon) ?>  <?= $page->title ?></h2>

                <?php if($page->getChildren()) { ?>
<div class="row">
       <?php foreach ($page->getChildren()->all()  as $key1 => $children) {?>
            <article>

                 <div class="col-md-4">
                <h4> <?= Html::a($children->title,['page/page', 'slug'=>$children->slug])?></h4>
                 <div class="border"></div>
                 <p><?= $children->description ?></p>
              </div>
              <?php if ( $key1%3== 2){?>
                  <div class="clearfix"></div>
              <?php } ?>
            </article>
        <?php } ?>
        </div>
        <?php } ?>
        </center>
            </div>
            </div>
             <?php } ?>
           
        

    </div>
</div>

<?php  


       // var_dump($arr);


