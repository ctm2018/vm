<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use app\models\Pages;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' =>Html::img('@web'.'/img/logo.jpg', ['alt'=>Yii::$app->name, 'height'=>'50px']).'<span>'. Yii::$app->name.'</span>',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);

    $items = [];
    $items [] =['label' => 'Главная', 'url' => ['/site/index']];
    foreach (Pages::find()->where(['in_menu'=> Pages::STATUS_ACTIVE, 'status'=> Pages::STATUS_ACTIVE])->limit(3)->all() as $value) {
        $items [] =['label' => $value->title, 'url' => ['/page/page', 'slug'=> $value->slug]];
    }
    $items [] = Yii::$app->user->isGuest ? '' : (
                '<li>'
                . Html::beginForm(['/site/logout'], 'post')
                . Html::submitButton(
                    'ВЫЙТИ (' . Yii::$app->user->identity->username . ')',
                    ['class' => 'btn btn-link logout']
                )
                . Html::endForm()
                . '</li>'
            );
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => $items
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        
 <div class="row row-offcanvas row-offcanvas-right">
      <div class="col-xs-12 col-sm-9">
         <p class="pull-right visible-xs">
            <button type="button" class="btn btn-primary btn-xs" data-toggle="offcanvas"> Меню</button>
          </p>
          <?= $content ?> 
</div>
 
        <div class="col-xs-6 col-sm-3 sidebar-offcanvas" id="sidebar" role="navigation">
   <?php
    echo Menu::widget([
         'activeCssClass' => 'active',
        'options' => ['class' => 'nav nav_page'],
    'items' => $menu
]);
} ?>


        </div>
</div>
</div>

    </div>

<footer class="footer">
    <div class="container">
        <div class="row">
             <div class="col-sm-4">
                <h4></h4>
                <p>&copy; <?= Yii::$app->name ." 2013-".date('Y') ?></p>
             </div>
             <div class="col-sm-4">
                  <h4>Мы на связи</h4>
                 <p>Телефон:44-36-73, 44-36-74 <br />
                 Email: sosh_gl@mail.grodno.by</p>
             </div>
             <div class="col-sm-4">
                <h4>Адрес</h4>
                 <p>230009 г. Гродно, ул. Сухомбаева 9</p>
             </div>
            
        </div>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
