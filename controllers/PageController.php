<?php
namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\readModels\PagesReadRepository;
use app\readModels\SiteReadRepository;
use yii\web\NotFoundHttpException;
/**
 * Page controller
 */

class PageController extends Controller
{
    /**
     * @inheritdoc
     */
    private $page;
    private $site;

    public function __construct($id, $module, PagesReadRepository $page,  SiteReadRepository $site, $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->site = $site;
        $this->page = $page;
    }


    public function actionPage($slug)
    {
        return $this->render('page', [
            'page' => $this->findModel($slug),
            'menu' => $this->site->findPageMenuAll() 
        ]);
    }
	
	protected function findModel ($slug) {
	     if (($page = $this->page->findOne($slug)) != null ) {
		    return $page; }
			else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
	
	}
	
	public function actionDistionary($slug, $id)
    {
	    if (!$ds = $this->findModel($slug)->getDictionary()->where(['id'=>$id])->one()) {
		  throw new NotFoundHttpException('The requested distionary does not exist.');
		}
        return $this->renderAjax('distionary', [
            'ds' => $ds,
        ]);
    }

    
}
