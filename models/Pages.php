<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use paulzi\nestedsets\NestedSetsBehavior;
use paulzi\nestedsets\NestedSetsQueryTrait;
use yii\behaviors\AttributeBehavior;
use rmrevin\yii\fontawesome\FA;
use lhs\Yii2SaveRelationsBehavior\SaveRelationsBehavior;
use yii\helpers\ArrayHelper;
use yii\helpers\Inflector;
use yii\db\ActiveRecord;
use app\components\gall\GalleryBehavior;

/**
 * This is the model class for table "pages".
 *
 * @property int $id
 * @property string $title
 * @property string $slug
 * @property string $content
 * @property int $status
 * @property int $in_menu
 * @property string $icon
 * @property int $lft
 * @property int $rgt
 * @property int $depth
 * @property int $created_at
 * @property int $updated_at
 *
 * @property Links[] $links
 * @property Peoples[] $peoples
 */
class Pages extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    const STATUS_DRAFT = 0;
    const STATUS_ACTIVE = 10;

    public $parentId;

     use NestedSetsQueryTrait;

    public static function tableName()
    {
        return 'pages';
    }

    public static function statusList()
    {
        return [
            self::STATUS_DRAFT => 'Не опубликован',
            self::STATUS_ACTIVE => 'Опубликован',
        ];
    }

    public static function statusMenuList()
    {
        return [
            self::STATUS_DRAFT => 'Нет',
            self::STATUS_ACTIVE => 'Да',
        ];
    }

     public static function statusName($status)
    {
        return ArrayHelper::getValue(self::statusList(), $status);
    }

    public static function statusMenuName($menu)
    {
        return ArrayHelper::getValue(self::statusMenuList(), $menu);
    }


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'parentId'], 'required'],
            [['content', 'description'], 'string'],
            [['status', 'lft', 'rgt', 'in_menu', 'depth', 'created_at', 'updated_at'], 'integer'],
            [['title', 'slug', 'icon'], 'string', 'max' => 255],
            [['slug'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Заголовок',
            'slug' => 'Slug',
            'description' => "Краткое описание",
            'parentId' => 'Родительская страница',
            'content' => 'Контент',
            'status' => 'Статус',
            'in_menu' => 'В меню?',
            'icon' => 'Иконка',
            'lft' => 'Lft',
            'rgt' => 'Rgt',
            'depth' => 'Depth',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата обновления',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLinks()
    {
        return $this->hasMany(Links::className(), ['page_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPeoples()
    {
        return $this->hasMany(Peoples::className(), ['page_id' => 'id']);
    }

    public  static function parentsList()
    {
        return ArrayHelper::map(self::find()->orderBy('lft')->asArray()->all(), 'id', function (array $page) {
            return ($page['depth'] > 1 ? str_repeat('-- ', $page['depth'] - 1) . ' ' : '') . $page['title'];
        });

    }

    public  static function parentsListNoRoot()
    {
        return ArrayHelper::map(self::find()->andwhere(['status' => self::STATUS_ACTIVE])->andWhere(['>', 'depth', 0])->orderBy('lft')->asArray()->all(), 'id', function (array $page) {
            return ($page['depth'] > 1 ? str_repeat('-- ', $page['depth'] - 1) . ' ' : '') . $page['title'];
        });

    }

    public function iconList() {
        $arr=[];
        $icons = (new \ReflectionClass(FA::class))->getConstants();
        foreach ($icons as $key => $value) {
              $arr[] = ["icons" => $value];
            
        }
        return ArrayHelper::map($arr, 'icons', 'icons');
    }

    public function behaviors()
    {
        return [
               'galleryBehavior' => [
             'class' => GalleryBehavior::className(),
             'type' => 'pages',
             'extension' => 'jpg',
             'directory' => Yii::getAlias('@webroot') . '/images/pages/gallery',
             'url' => Yii::getAlias('@web') . '/images/pages/gallery',
             'versions' => [
                 'small' => function ($img) {
                     /** @var \Imagine\Image\ImageInterface $img */
                     return $img
                         ->copy()
                         ->thumbnail(new \Imagine\Image\Box(246, 226));
                 },
                 'medium' => function ($img) {
                     /** @var \Imagine\Image\ImageInterface $img */
                     $dstSize = $img->getSize();
                     $maxWidth = 800;
                     if ($dstSize->getWidth() > $maxWidth) {
                         $dstSize = $dstSize->widen($maxWidth);
                     }
                     return $img
                         ->copy()
                         ->resize($dstSize);
                 },
             ]
         ],
              [
              'class' => AttributeBehavior::className(),
              'attributes' => [
                  ActiveRecord::EVENT_BEFORE_INSERT => 'slug',
                  ActiveRecord::EVENT_BEFORE_UPDATE => 'slug',
              ],
              'value' => function ($event) {
                  return Inflector::slug($this->title,'_');
              },
              ],

              [
              'class' => AttributeBehavior::className(),
              'attributes' => [
                  ActiveRecord::EVENT_BEFORE_INSERT => 'status',
              ],
              'value' => function ($event) {
                  return self::STATUS_DRAFT;
              },
              ],

              [
              'class' => AttributeBehavior::className(),
              'attributes' => [
                  ActiveRecord::EVENT_BEFORE_INSERT => 'in_menu',
              ],
              'value' => function ($event) {
                  return self::STATUS_DRAFT;
              },
              ],



                [
                'class' => NestedSetsBehavior::className(),
                ],
                TimestampBehavior::className(),
           
               // [
               //  'class' => SaveRelationsBehavior::className(),
               //  'relations' => ['tagPage', 'soursePage', 'sourseImgPage', 'youtube', 'dictionary', 'dictionaryOne'],
               // ],
            
        ];
    }

    public function transactions()
    {
        return [
            self::SCENARIO_DEFAULT => self::OP_ALL,
        ];
    }

}
