<?php

namespace app\modules\admin\controllers;

use Yii;
use app\models\Pages;
use app\modules\admin\search\PagesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\components\gall\GalleryManagerAction;
use yii\filters\AccessControl;


/**
 * PagesController implements the CRUD actions for Pages model.
 */
class PagesController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public $layout ="main";

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'create', 'update', 'delete', 'view','active', 'draft', 'nomenu', 'inmenu'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                     'draft' => ['POST'],
                      'active' => ['POST'],
                       'nomenu' => ['POST'],
                        'inmenu' => ['POST'],
                ],
            ],
        ];
    }

    public function actions()
{
    return [
       'galleryApi' => [
           'class' => GalleryManagerAction::className(),
           // mappings between type names and model classes (should be the same as in behaviour)
           'types' => [
               'pages' => Pages::className()
           ]
       ],
    ];
}

    /**
     * Lists all Pages models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PagesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Pages model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Pages model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Pages();
        if ($model->load(Yii::$app->request->post())) {
        
            $model->appendTo($this->findModel($model->parentId));
            $model->save(false);
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Pages model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->parentId =  $model->parent->id;

        if ($model->load(Yii::$app->request->post())) {
            if ($model->parentId !== $model->parent->id) {
            $model->appendTo($this->findModel($model->parentId));
        }  
         $model->save();
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Pages model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $this->assertIsNotRoot($model);
        $model->delete();
        return $this->redirect(['index']);
    }

       public function actionDraft($id)
    {
        $model=$this->findModel($id);
        $model->status = Pages::STATUS_DRAFT;
        $model->save(false);
        return $this->redirect(['view', 'id' => $model->id]);
    }

      public function actionActive($id)
    {
        $model=$this->findModel($id);
        if ($model->depth == 1 || $model->content || count($model->getBehavior('galleryBehavior')->getImages())) {  
        $model->status = Pages::STATUS_ACTIVE;
        $model->save(false);
        
    } else {
         \Yii::$app->getSession()->setFlash('error', 'Не может быть опубликована страница, так как ее контент пустой или нет ни одной картинки!');}
        return $this->redirect(['view', 'id' => $model->id]);
    }


      public function actionNomenu($id)
    {
        $model=$this->findModel($id);
        $model->in_menu = Pages::STATUS_DRAFT;
        $model->save(false);
        return $this->redirect(['view', 'id' => $model->id]);
    }

      public function actionInmenu($id)
    {
        $model=$this->findModel($id);
        if ($model->content) {  
        $model->in_menu = Pages::STATUS_ACTIVE;
        $model->save(false);  
    } else {
       \Yii::$app->getSession()->setFlash('error', 'Не может быть дабавлена страница в меню , так как ее контент пустой!');}
        return $this->redirect(['view', 'id' => $model->id]);
    }


    /**
     * Finds the Pages model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Pages the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Pages::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('Такой страницы нет');
    }

     private function assertIsNotRoot(Pages $page)
    {
        if ($page->isRoot()) {
            throw new \DomainException('Не удается управлять корневой страницей.');
        }
    }
}
