<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Links */

$this->title = 'Добавить новую ссылку';
$this->params['breadcrumbs'][] = ['label' => 'Cсылки', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="links-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
