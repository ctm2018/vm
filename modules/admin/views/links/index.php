<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\grid\GridView;
use app\models\Pages;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\search\LinksSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ссылки';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="links-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Добавить новую ссылку', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

           // 'id',
            ['attribute' => 'page_id',
             'value' => 'page.title',
             'filter' => ArrayHelper::map(Pages::find()->where(['status'=> Pages::STATUS_ACTIVE, 'in_menu'=> Pages::STATUS_DRAFT, 'depth' =>2])->orderBy(['lft' => SORT_ASC])->all(), 'id', 'title')
            ],
            'title',
            'link',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
