<?php
use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $content string */
?>

<header class="main-header">

    <?= Html::a('<span class="logo-mini">ВМ</span><span class="logo-lg">' . Yii::$app->name . '</span>', Yii::$app->homeUrl, ['class' => 'logo']) ?>

    <nav class="navbar navbar-static-top" role="navigation">

        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>

        <div class="navbar-custom-menu">

            <ul class="nav navbar-nav">
                                <?= '<li>'
                . Html::beginForm(['/site/logout'], 'post')
                . Html::submitButton(
                    'ВЫЙТИ (' . Yii::$app->user->identity->username . ')',
                    ['class' => 'btn btn-link logout', "style"=>["color" => "white"]]
                )
                . Html::endForm()
                . '</li>'?>

                
            </ul>
        </div>
    </nav>
</header>
