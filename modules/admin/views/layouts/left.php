<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?= $directoryAsset ?>/img/user2-160x160.jpg" class="img-circle" alt="User Image"/>
            </div>
            <div class="pull-left info">
                <p  style="color:white">Админ</p>

                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <!-- search form -->

        <!-- /.search form -->

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                'items' => [
                    ['label' => 'Меню', 'options' => ['class' => 'header']],
                    // ['label' => 'Login', 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest],
                    [
                        'label' => 'Страницы',
                        'icon' => 'file-text',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Просмотр', 'icon' => 'table', 'url' => ['/admin/pages/index'],],
                            ['label' => 'Создать', 'icon' => 'edit', 'url' => ['/admin/pages/create'],],
            
                        ],
                    ],

                    [
                        'label' => 'Ссылки',
                        'icon' => 'link',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Просмотр', 'icon' => 'table', 'url' => ['/admin/links/index'],],
                            ['label' => 'Создать', 'icon' => 'edit', 'url' => ['/admin/links/create'],],
            
                        ],
                    ],
                ],
            ]
        ) ?>

    </section>

</aside>
