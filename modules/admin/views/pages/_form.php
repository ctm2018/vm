<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use mihaildev\ckeditor\CKEditor;
use mihaildev\elfinder\ElFinder;
use kartik\select2\Select2;
use yii\web\JsExpression; 
use yii\web\View; 

/* @var $this yii\web\View */
/* @var $model app\models\Pages */
/* @var $form yii\widgets\ActiveForm */

// Templating example of formatting each list element
$format = <<< SCRIPT
function format(state) {
    return '<i class="fa fa-'+state.text+'"></i>  ' + state.text;
}
SCRIPT;
 $escape = new JsExpression("function(m) { return m; }");
  $this->registerJs($format, View::POS_HEAD);
?>
<div class="pages-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
    
    <?= $form->field($model, 'description')->textarea() ?>

   <?= $form->field($model, 'content')->widget(CKEditor::className(), [
  'editorOptions' => ElFinder::ckeditorOptions('elfinder',[/* Some CKEditor Options */'allowedContent' => true]),
]); ?>


    <?= $form->field($model, 'parentId')->dropDownList($model::parentsList()) ?>

    <?= $form->field($model, 'icon')->widget(Select2::classname(),[
    'data' => $model->iconList(),
    'options' => ['placeholder' => 'Выберите иконку ...'],
    'pluginOptions' => [
           'templateResult' =>  new JsExpression('format'),
        'templateSelection' =>  new JsExpression('format'),
         'escapeMarkup' => $escape,
        'allowClear' => true
    ],
]); ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
