<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use rmrevin\yii\fontawesome\FA;
use app\components\gall\GalleryManager;


/* @var $this yii\web\View */
/* @var $model app\models\Pages */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Страницы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$d="";
foreach ($model->parents as $parent) {
           if (!$parent->isRoot()) {
            $d.=  Html::a($parent->title, ['view', 'id' => $parent->id]) . '/'; }}
?>
<div class="pages-view">


    <p>
        <?= Html::a('Обновить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены, что хотите удалить страницу?',
                'method' => 'post',
            ],
        ]) ?>
       <?php if ($model->status) { ?>
        <?= Html::a('Не опубликовать', ['draft', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены, что хотите не опубликовать страницу?',
                'method' => 'post',
            ],
        ]) ?>
          <?php } else { ?>
        <?= Html::a('Опубликовать', ['active', 'id' => $model->id], [
            'class' => 'btn btn-success',
            'data' => [
                'confirm' => 'Вы уверены, что хотите опубликовать страницу?',
                'method' => 'post',
            ],
        ]) ?>

          <?php } if ($model->in_menu) { ?>
        <?= Html::a('Снять с меню', ['nomenu', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены, что хотите убарть страницу с меню?',
                'method' => 'post',
            ],
        ]) ?>
            <?php } else { ?>
        <?= Html::a('Доабвить в меню', ['inmenu', 'id' => $model->id], [
            'class' => 'btn btn-success',
            'data' => [
                'confirm' => 'Вы уверены, что хотите добавить страницу в меню?',
                'method' => 'post',
            ],
        ]) ?>

          <?php  } ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'title',
            'description:ntext',
            'content:raw',
             ['label'=>$model->getAttributeLabel('status'), 
             'value' => $model::statusName($model->status)],
             ['label'=>$model->getAttributeLabel('in_menu'), 
             'value' => $model::statusMenuName($model->in_menu)],
              ['format' => 'raw',
                'label'=>$model->getAttributeLabel('icon'), 
             'value' =>  FA::icon($model->icon).' '. $model->icon],
             [ 'format' => 'raw',
                'label' => "Родительскте страницы",
              'value' => $d],
            'created_at:date',
            'updated_at:date',
        ],
    ]) ?>
<?php
    echo GalleryManager::widget(
        [
            'model' => $model,
            'behaviorName' => 'galleryBehavior',
            'apiRoute' => 'pages/galleryApi'
        ]
    );
?>
</div>
