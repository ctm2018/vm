<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\Pages;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\searchPagesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Страницы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pages-index">
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Создать новую страницу', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'title',
            'description:ntext',
            ['attribute'=>'status',
              'value' => function($model) {
               return Pages::statusName($model->status);
              },
              'filter'=> $searchModel::statusList(),
            ],
            ['attribute'=>'in_menu',
              'value' => function($model) {
               return Pages::statusMenuName($model->in_menu);
              },
              'filter'=> $searchModel::statusMenuList(),
            ],
            //'in_menu',
            //'icon',
            //'lft',
            //'rgt',
            //'depth',
            'created_at:date',
            'updated_at:date',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
