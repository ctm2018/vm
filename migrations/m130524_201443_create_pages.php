<?php

use yii\db\Migration;

/**
 * Handles the creation of table `pages`.
 */
class m130524_201443_create_pages extends Migration
{
    public function up()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';

        $this->createTable('{{%pages}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string()->notNull(),
            'slug' => $this->string()->notNull(),
            'description' => $this->string()->notNull(),
            'content' => 'MEDIUMTEXT',
            'status' => $this->integer()->notNull(),
            'in_menu' => $this->integer()->notNull(),
            'icon' => $this->string()->notNull(),
            'lft' => $this->integer()->notNull(),
            'rgt' => $this->integer()->notNull(),
            'depth' => $this->integer()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
             ], $tableOptions);
          $this->createIndex('{{%idx-pages-slug}}', '{{%pages}}', 'slug', true);

        $this->insert('{{%pages}}', [
            'id' => 1,
            'title' => '',
            'slug' => 'root',
            'description' => '',
            'content' => null,
            'status' => 0,
            'in_menu' => 0,
            'icon'=> '',
            'lft' => 1,
            'rgt' => 2,
            'depth' => 0,
            'created_at' => 0,
            'updated_at' => 0,
        ]);
    }

    public function down()
    {
        // echo "Mii";
        // return false;
        $this->dropTable('{{%pages}}');
    }
}
