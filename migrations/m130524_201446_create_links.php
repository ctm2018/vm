<?php

use yii\db\Migration;

/**
 * Handles the creation of table `links`.
 */
class m130524_201446_create_links extends Migration
{
    public function up()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';

        $this->createTable('{{links}}', [
            'id' => $this->primaryKey(),
            'page_id' => $this->integer(11)->notNull(),
            'title' => $this->string()->notNull(),
            'link' =>$this->string()->notNull(),
             ], $tableOptions); 

    
        
        $this->createIndex('{{%idx-pages}}', '{{%links}}', 'page_id');
        $this->addForeignKey('{{%fk-idx-pages}}', '{{%links}}', 'page_id', '{{%pages}}', 'id',  'CASCADE', 'RESTRICT');
}



    public function down()
    {
        $this->dropTable('{{%links}');
    }
}
