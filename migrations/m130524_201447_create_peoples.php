<?php

use yii\db\Migration;

/**
 * Handles the creation of table `peoples`.
 */
class m130524_201447_create_peoples extends Migration
{
    public function up()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';

        $this->createTable('{{peoples}}', [
            'id' => $this->primaryKey(),
            'page_id' => $this->integer(11)->notNull(),
            'name' => $this->string()->notNull(),
            'content' => $this->string()->notNull(),
            'image' =>  $this->string()->notNull(),
             ], $tableOptions); 

    
        
        $this->createIndex('{{%idx-pages2}}', '{{%peoples}}', 'page_id');
        $this->addForeignKey('{{%fk-idx-pages2}}', '{{%peoples}}', 'page_id', '{{%pages}}', 'id',  'CASCADE', 'RESTRICT');
}



    public function down()
    {
        $this->dropTable('{{%peoples}}');
    }
}
