<?php

namespace app\readModels;

use app\models\Pages;


class SiteReadRepository
{

    public function findPagesLimit()
    {
        $pages = Pages::find()->where(['status'=> Pages::STATUS_ACTIVE, 'in_menu'=> Pages::STATUS_DRAFT, 'depth' =>1])->orderBy(['lft' => SORT_ASC])->all();
        return $pages;
    }

    public function findPagesDateUpdate()
    {
        $pages = Pages::find()->where(['status'=> Pages::STATUS_ACTIVE])->andWhere(['depth' => [1,2] ])->andWhere(['!=','content', ''])->orderBy(['updated_at' => SORT_DESC])->limit(2)->all();
        return $pages;
    }


    public function findPageMenuAll()
    {
        $menuItems =[];
        $menues = Pages::find()->where(['status'=> Pages::STATUS_ACTIVE, 'in_menu'=> Pages::STATUS_DRAFT, 'depth' =>1])->all(); 
        foreach ($menues as $key => $menu) {
        $menuItems[$key] = ['label' => $menu->title,  'options' => ['class' => 'nav-header'], 'items' =>$this->findPageMenuDrop($menu->id)
    ]; }

       return $menuItems;    
    }  

    
    protected function findPageMenuDrop($id) {
     $items=[];
      $models = Pages::findOne($id);
      foreach ($models->getChildren()->all() as $children)  {
         $items[]= ['label' => $children->title, 'url' => ['page/page', 'slug' => $children->slug ]];
        } 
        return $items;
    }



}